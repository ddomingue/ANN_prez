# Introduction aux réseaux de neurones artificiels

#HSLIDE
## Contexte : apprentissage statistique

- Problèmes complexes pour algorithmes classiques 
	- Pas de programmation "explicite"
- Fournir un modèle permettant de simplifier la complexité 
- Adaptation aux données


Reconnaissance des formes, Filtrage, Classification, Prédiction, ...

[//]: # (de caractères, de visages, de signaux accoustiques)
[//]: # (filtrage de spams)
[//]: # (prévision de consommation d'eau, de trafic routier, de cours boursiers)
[//]: # (commande de véhicules)

#HSLIDE
# PLAN

- Historique
- Structure
- Utilisation

#HSLIDE
# HISTORIQUE

#HSLIDE
## Les débuts des réseaux de neurones

|   | |
|---------|----------------|
| 1943 : Le modèle McCulloch-Pitts | Analogie entre neurones et fonctions logiques |
| 1949 : La règle de Hebb  | "Cells that fire together, wire together" |
| 1957 : Frank Rosenblatt (le perceptron)  | Fonctions booléennes par apprentissage |

[//]: # (Biomimétisme)

#HSLIDE 
## Modèle d'un neurone

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/perceptron_neuron.png" alt="Modèle du neurone" style="width:90%; height:60%" />

#HSLIDE
## Le neurone formel

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/perceptron_formal.png" alt="Neurone formel" style="width:100%; height:100%" />

[//]: # (<img src="resources/perceptron_neuron.png" alt="Modèle du neurone" style="width:50%; height:50%" />)
[//]: # (<img src="resources/perceptron_rosenblatt.png" alt="Neurone formel" style="width:50%; height:50%" />)

#HSLIDE
## Les fonctions d'activation

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/activation_functions.png" alt="Activateurs" style="width:80%; height:80%" />

#HSLIDE
## Les années 60-90

|   | |
|---------|----------------|
| 1960 : Widrow et Hoff | (M)ADALINE : filtrage des echos |
| 1969 : Perceptrons, Minsky et Papert | Limites du perceptron |
| 1974/1982 : Werbos <br/> 1985 : LeCun  <br/> 1986 : Rumelhart  | Propagation arrière multi-couches
| 1989 : Cybenko | Sigmoïdes, approximateurs univ. 

[//]: # (XOR impossible à apprendre, limitation aux séparations linéaires)
[//]: # (Progrès des ordinateurs traitement symbolique, systèmes experts)
[//]: # (Backpropagation : apprentissage des réseaux à couches cachées)

#HSLIDE
## Les années 2000

- Augmentation des capacités de traitement  
- Réduction du coût des processeurs, GPUs  
- Augmentation du volume des données  

- Les réseaux convolutionnels  
- Deep learning 

[//]: # (2014 AlphaGO)

#HSLIDE

# Structure

#HSLIDE
### Structure : Catégories

Une multitude de topologies existe de nos jours

Deux grandes catégories :   
- réseaux non bouclés (feed forward networks) 
	- classification, reconnaissance des formes, prédiction
- réseaux bouclés (recurrent networks)  
	- mémoire associative, traitement du signal, commande

#HSLIDE
## Structure : feed forward

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/structure_1.png" alt="Exemple ANN" style="width:70%; height:70%" />

#HSLIDE
## Structure : matrices

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/matrices.png" alt="Matrices" style="width:60%; height:60%" />

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/matrices2.png" alt="Matrices" style="width:40%; height:40%" />

#HSLIDE

# Utilisation

#HSLIDE
## Utilisation

Deux modes d'utilisation
- apprentissage
	- Apprendre une relation entre entrées et sortie(s) pour une tâche donnée
	- Vérification de l'apprentissage
- reconnaissance
	- Calcul de la (les) sorties par rapport aux entrées


#HSLIDE 
## Utilisation : Reconnaissance

Forward propagation ( propagation avant )

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/structure_forward.png" alt="Propagation avant" style="width:80%; height:80%" />

#HSLIDE
## Utilisation : Apprentissage

- Augmenter la performance d'un modèle sur une tâche T avec de l'expérience
- Données d'entraînement
- Apprentissage supervisé : on donne les réponses attendues
- Ajuster les poids dans le réseau
- Tester des configurations du réseau
#HSLIDE
## Utilisation : Appr. supervisé

Données d'entraînement  
<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/spam_data.jpg" alt="Donnees app. supervise" style="width:60%; height:60%" />

Apprentissage (60), validation (20), tests (20)


#HSLIDE
## Utilisation : Appr. supervisé

La notion d'erreur

La fonction de coût

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/mse.png" alt="Mean Square Error" style="width:75%; height:75%" />

On veut minimiser le coût : descente de gradient + coef. apprentissage

#HSLIDE
## Utilisation : Appr. supervisé 


<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/gradient_descent.png" alt="Descente de gradient" style="width:50%; height:50%" />
<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/weight_update.png" alt="Maj poids" style="width:50%; height:50%" />

#HSLIDE
## Utilisation : Appr. supervisé

Minimiser le coût : dans un réseau de neurones multi-couches

L'algorithme de propagation arrière
- Propager la minimisation du coût à l'ensemble des couches d'un réseau
- A partir du côut
- En utilisant la règle de dérivation des fonctions composées (chain rule)

#HSLIDE
## Utilisation : Appr. supervisé

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/backpropagation.png" alt="Backprop" style="width:60%; height:60%" />

<img src="https://gitlab.com/ddomingue/ANN_prez/raw/master/resources/chain_rule.png" alt="Backprop" style="width:50%; height:50%" />

#HSLIDE
## Utilisation : Vérification

- Validation
	- Calibrer la structure du modèle
	- Tester différentes configurations

- Tests
	- Vérifier que la sortie attendue correspond à la sortie désirée

#HSLIDE
# CONCLUSION


#HSLIDE
# Questions